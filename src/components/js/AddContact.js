import axios from 'axios'

export default {
    name: 'AddPage',
    data () {
      return {
        msg: 'This is Add Contact Page',
        Data:[
          {
          contactID: '',
          firstName: '',
          lastName: '',
          gender: '',
          email: '',
          mobile: '',
          facebook:'',
          imageURL:'',
          }
        ],
      }
    },
    methods: {
      async addContact(){
        console.log(this.Data)
        let newData = {
          contactID: this.Data.contactID,
          firstName: this.Data.firstName,
          lastName: this.Data.lastName,
          gender: this.Data.gender,
          email: this.Data.email,
          mobile: this.Data.mobile,
          facebook:this.Data.facebook,
          imageURL:this.Data.imageURL,
        }
        console.log(newData)
        await axios.post('http://localhost:3000/contacts/add/',newData)
        .then((res) =>{
          this.Data = res.data
          console.log(this.Data)
        })
        window.location.href='http://localhost:8081/#/Home/'
        return newData
        // window.location.reload()
      },
  }
}